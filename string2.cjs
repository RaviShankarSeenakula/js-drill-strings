// ==== String Problem #2 ====
// Given an IP address - "111.139.161.143". 
// Split it into its component parts 111, 139, 161, 143 and return it in an array in numeric values. [111, 139, 161, 143].

// Support IPV4 addresses only. If there are other characters detected, return an empty array.

const string1 = "111.139.161.143";

function string2(IP) {
    let result = []; 
    let values = IP.split('.');
    for (let index = 0; index < values.length; index++) {
        val = Number(values[index]);
        if (!isNaN(val)) {
            result.push(val)
        } else {
            return [];
        }
    }
    return result;
}

// console.log(string2(string1));

module.exports = string2;