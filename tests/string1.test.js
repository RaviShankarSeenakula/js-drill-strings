const string1 = require("../string1.cjs");

const moneyArray = [ "$100.45", "$1,002.22", "-$123" ];

test("Testing Problem1", () => {
    expect(string1(moneyArray)).toStrictEqual([ 100.45, 1002.22, -123 ])
});

const moneyArray2 = [ "$100.45", "$1,00.2.22", "-$123" ];

test("Testing Problem1", () => {
    expect(string1(moneyArray2)).toStrictEqual([ 100.45, 0, -123 ])
});