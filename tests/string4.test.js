
const string4 = require("../string4.cjs");

const name = {"first_name": "JoHN", "last_name": "SMith"};

test("Testing Problem4", () => {
    expect(string4(name)).toStrictEqual('John Smith')
});

const name2 = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};

test("Testing Problem4", () => {
    expect(string4(name2)).toStrictEqual('John Doe Smith')
});