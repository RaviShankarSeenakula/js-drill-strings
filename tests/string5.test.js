
const string5 = require("../string5.cjs");

const stringArray = ["the", "quick", "brown", "fox"];


test("Testing Problem5", () => {
    expect(string5(stringArray)).toStrictEqual('the quick brown fox')
});
