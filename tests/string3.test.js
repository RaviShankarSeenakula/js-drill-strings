const string3 = require("../string3.cjs");

const date = "10/01/2021";

test("Testing Problem3", () => {
    expect(string3(date)).toStrictEqual('January')
});

const date2 = "30/12/2023";

test("Testing Problem3", () => {
    expect(string3(date2)).toStrictEqual('December')
});