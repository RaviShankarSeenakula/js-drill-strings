const string2 = require("../string2.cjs");

const ipAddress = "111.139.161.143";

test("Testing Problem2", () => {
    expect(string2(ipAddress)).toStrictEqual([ 111, 139, 161, 143 ])
});

const ipAddress2 = "111.139.161.14g3";

test("Testing Problem2", () => {
    expect(string2(ipAddress2)).toStrictEqual([])
});