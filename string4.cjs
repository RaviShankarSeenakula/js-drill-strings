// ==== String Problem #4 ====
// Given an object in the following format, return the full name in title case.
// {"first_name": "JoHN", "last_name": "SMith"}
// {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"}

const name = {"first_name": "JoHN", "last_name": "SMith"};
const name1 = {"first_name": "JoHN", "middle_name": "doe", "last_name": "SMith"};


function string4(obj) {
    let fullName = '';
    for (item in obj) {
        fullName += obj[item][0].toUpperCase() + obj[item].slice(1).toLowerCase()  + ' ';
    }
    return fullName.trim();
}

// console.log(string4(name));
// console.log(string4(name1));


module.exports = string4;
