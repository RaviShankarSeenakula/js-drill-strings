// ==== String Problem #1 ====
// There are numbers that are stored in the format "$100.45", "$1,002.22", "-$123", and so on. 
// Write a function to convert the given strings into their equivalent numeric format without any precision loss - 100.45, 1002.22, -123 and so on. 
// There could be typing mistakes in the string so if the number is invalid, return 0.

const elements = ["$100.45", "$1,00.2.22", "-$123"];

function string1(elements) {
    const result = [];
    for (let index = 0; index < elements.length; index++) {
        let value = elements[index].replace('$','');
        value = value.replaceAll(',','');
        value = Number(value);
        if (!isNaN(value)) {
            result.push(value);
        } else {
            result.push(0);
        }
    }
    return result;
}

// console.log(string1(elements));

module.exports = string1;