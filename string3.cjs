// ==== String Problem #3 ====
// Given a string in the format of "10/1/2021", print the month in which the date is present in.

const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];

const date = "10/10/2021";

function string3(date) {
    date = date.split('/');
    return months[Number(date[1]) - 1]
}

// console.log(string3(date));

module.exports = string3;