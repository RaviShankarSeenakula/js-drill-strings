// ==== String Problem #5 ====
// Given an array of strings ["the", "quick", "brown", "fox"], convert it into a string "the quick brown fox."
// If the array is empty, return an empty string.

const stringArray = ["the", "quick", "brown", "fox"];

function string5(stringArray) {
    let sentence = '';
    for (let index = 0; index < stringArray.length; index++) {
        sentence += stringArray[index] + ' ';
    }
    return sentence.trim();
}

// console.log(string5(stringArray));

module.exports = string5;